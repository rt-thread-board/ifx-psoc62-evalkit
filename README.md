# IFX-PSoC62-EvalKit

文件描述：


- IFX-PSoC6-RT-Thread-sch.pdf：开发板原理图
- Cypress.PSoC6_DFP.1.2.0.pack：芯片的 MDK 安装包

如要下载最新的MDK芯片支持包，可前往：https://github.com/Infineon/cmsis-packs/tree/master/PSoC6_DFP

RT-Thread 源码仓库下的 BSP 地址：[https://github.com/RT-Thread/rt-thread/tree/master/bsp/Infineon/psoc6-evaluationkit-062S2](https://github.com/RT-Thread/rt-thread/tree/master/bsp/Infineon/psoc6-evaluationkit-062S2)

RT-Thread 文档中心开发板介绍：[https://www.rt-thread.org/document/site/#/rt-thread-version/rt-thread-standard/hw-board/ifx-eval-kit/ifx-eval-kit](https://www.rt-thread.org/document/site/#/rt-thread-version/rt-thread-standard/hw-board/ifx-eval-kit/ifx-eval-kit)

